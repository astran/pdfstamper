module com.ace.pdfwatermark.pdfwatermark {
    requires javafx.controls;
    requires javafx.fxml;
    requires kotlin.stdlib;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.apache.pdfbox;
    requires org.apache.pdfbox.tools;
    requires java.desktop;
    requires javafx.swing;
    requires fop;
    requires batik.transcoder;
    requires kotlin.stdlib.jdk8;

    opens com.ace.pdfwatermark.pdfwatermark.gui to javafx.fxml;
    exports com.ace.pdfwatermark.pdfwatermark;
    exports com.ace.pdfwatermark.pdfwatermark.gui;
    exports com.ace.pdfwatermark.pdfwatermark.watermark;
    opens com.ace.pdfwatermark.pdfwatermark.watermark to javafx.fxml;
}
