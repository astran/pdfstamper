package com.ace.pdfwatermark.pdfwatermark.watermark

import com.ace.pdfwatermark.pdfwatermark.AppConfig
import com.ace.pdfwatermark.pdfwatermark.image.ImageToPdf
import com.ace.pdfwatermark.pdfwatermark.shortSha1
import java.io.File


class WatermarkCreator {
    fun convertWatermarkIfNeeded(watermarkFileImage: File): File {
        return if (watermarkFileImage.extension == "pdf") {
            watermarkFileImage
        } else {
            createWatermarkFromPng(watermarkFileImage)
        }
    }


    fun createWatermarkFromPng(imageFile: File): File {

        val fileChecksum = imageFile.shortSha1()
        println(fileChecksum)
        AppConfig.tmpDir.mkdirs()
        val pdfImage = File(AppConfig.tmpDir, "$fileChecksum.pdf")
        if (pdfImage.exists()) {
            return pdfImage
        }
        if (imageFile.extension == "svg") {
            ImageToPdf().createPdfFromSvg(imageFile, pdfImage)
        } else {
            ImageToPdf().createPdfFromImages(pdfImage, listOf(imageFile))
        }

        println(pdfImage.absolutePath)
        return pdfImage
    }


}
