package com.ace.pdfwatermark.pdfwatermark.watermark

import java.io.File

data class WatermarkedPdf(

    val source: File,
    val sourceDirectory:File,
    val pdfWithWatermark: File? = null,
    val watermarkedImages: List<File>? = null,
    val pdfImageWithWatermark: File? = null
) {

 fun relativeFileName()=source.absolutePath.replace(sourceDirectory.absolutePath, "").replace(".pdf", "")
}
