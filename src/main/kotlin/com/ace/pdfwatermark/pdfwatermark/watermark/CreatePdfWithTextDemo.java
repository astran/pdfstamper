package com.ace.pdfwatermark.pdfwatermark.watermark;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.state.PDExtendedGraphicsState;
import org.apache.pdfbox.util.Matrix;

import java.awt.*;
import java.io.IOException;

/**
 * Creates a sample.pdf document and write a message at an offset with HELVETICA_BOLD font style.
 */
public class CreatePdfWithTextDemo {
    public static void main(String[] args) throws IOException {
        String filename = "sample.pdf";
        String message = "Réservé à la location";

        PDDocument doc = new PDDocument();
        try {
            PDPage page = new PDPage();
            doc.addPage(page);

            float height = page.getMediaBox().getHeight();
            float width = page.getMediaBox().getWidth();
            PDFont font = PDType1Font.HELVETICA_BOLD;

            float pageWidth = page.getMediaBox().getWidth();
            float pageHeight = page.getMediaBox().getHeight();
            float lineWidth = Math.max(pageWidth, pageHeight) / 1000;
            float markerRadius = lineWidth * 10;
            float fontSize = Math.min(pageWidth, pageHeight) / 20;
            float fontPadding = Math.max(pageWidth, pageHeight) / 100;

            PDExtendedGraphicsState graphicsState = new PDExtendedGraphicsState();
            graphicsState.setNonStrokingAlphaConstant(0.3f);
            graphicsState.setStrokingAlphaConstant(0.3f);

            PDPageContentStream contents = new PDPageContentStream(doc, page);
            contents.setFont(font, 50);
            contents.setGraphicsStateParameters(graphicsState);
            float charWidth = 18;//17
            int messageLength = message.length();

            float expectedSize = fontSize * messageLength;
            contents.transform(Matrix.getRotateInstance(0, 0, 10));

            contents.beginText();
            contents.setNonStrokingColor(Color.RED);
            contents.setStrokingColor(Color.RED);
            contents.showText(message);
            contents.endText();


            contents.close();
            doc.save(filename);
        } finally {
            doc.close();
        }
    }


}
