package com.ace.pdfwatermark.pdfwatermark.gui

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.stage.Stage

class PdfWatermarkApplication : Application() {
    override fun start(stage: Stage) {
        val fxmlLoader = FXMLLoader(PdfWatermarkApplication::class.java.getResource("main-view.fxml"))
        val scene = Scene(fxmlLoader.load(), 800.0, 600.0)
        stage.title = "Hello!"
        stage.scene = scene
        stage.show()
    }
}

fun main() {
    Application.launch(PdfWatermarkApplication::class.java)
}
