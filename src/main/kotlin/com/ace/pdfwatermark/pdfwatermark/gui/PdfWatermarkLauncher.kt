package com.ace.pdfwatermark.pdfwatermark.gui

import javafx.application.Application

//use launcher class to avoid the need of modules
fun main() {
    Application.launch(PdfWatermarkApplication::class.java)
}
