package com.ace.pdfwatermark.pdfwatermark.gui

import com.ace.pdfwatermark.pdfwatermark.AppConfig
import com.ace.pdfwatermark.pdfwatermark.PdfDirConverter
import com.ace.pdfwatermark.pdfwatermark.WaterMarker
import com.ace.pdfwatermark.pdfwatermark.image.PdfToImageRenderer
import com.ace.pdfwatermark.pdfwatermark.watermark.WatermarkCreator
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.concurrent.Task
import javafx.concurrent.Worker
import javafx.embed.swing.SwingFXUtils
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.ChoiceBox
import javafx.scene.control.Label
import javafx.scene.control.ListView
import javafx.scene.image.ImageView
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox
import javafx.stage.DirectoryChooser
import javafx.stage.FileChooser
import javafx.stage.Window
import java.awt.image.BufferedImage
import java.io.File
import java.net.URL
import java.util.*
import java.util.concurrent.Executors


class FileSmallDisplay(file: File) : File(file.absolutePath) {
    override fun toString(): String {
        return super.getName()
    }
}

class RelativeFileName(file: File, val dir: File) : File(file.absolutePath) {
    override fun toString(): String {
        return super.getAbsolutePath().replace(dir.absolutePath + "/", "")
    }
}

class MainController : Initializable {

    private val pdfDirConverter = PdfDirConverter()
    val newSingleThreadExecutor = Executors.newSingleThreadExecutor()

    @FXML
    private lateinit var pdfPreviewImageView: ImageView

    @FXML
    private lateinit var watermarkFile: Label

    @FXML
    private lateinit var pdfListView: ListView<File>

    @FXML
    private lateinit var watermarkFileSelect: ChoiceBox<File>

    @FXML
    private lateinit var leftPane: Pane

    @FXML
    private lateinit var rightPane: Pane

    @FXML
    private lateinit var vboxPane: VBox


    val fileChooser = FileChooser()
    val directoryChooser = DirectoryChooser()
    var watermarkPdf: File? = null
    var pdfDirs: File? = null

    val pdfFiles: MutableList<File> = mutableListOf()
    val observablePdfList = FXCollections.observableArrayList<File>()

    @FXML
    override fun initialize(p0: URL?, p1: ResourceBundle?) {
//        val parent:Pane = pdfPreviewImageView.parent as Pane
        configPreviewImage()

        configWatermarkSelect()
        vboxPane.minHeightProperty().bind(leftPane.heightProperty())
        vboxPane.minWidthProperty().bind(leftPane.widthProperty())
        pdfListView.minWidthProperty().bind(leftPane.widthProperty().multiply(0.70))

        pdfListView.selectionModel.selectedItemProperty()
            .addListener { _: ObservableValue<out File>, _: File?, newFile: File? ->
                if (newFile != null) {
                    renderPdfFile(newFile)
                }

            }
    }

    private fun configWatermarkSelect() {
        val elements = AppConfig.workDir.listFiles()
        watermarkFileSelect.items.addAll(elements.map { FileSmallDisplay(it) })
        watermarkFileSelect.selectionModel.select(0)
        watermarkFileSelect.selectionModel.selectedItemProperty()
            .addListener { _: ObservableValue<out File>, _: File?, newFile: File? ->
                if (newFile != null) {
                   // renderPdfFile(newFile)
                }

            }
    }

    private fun configPreviewImage() {
        pdfPreviewImageView.fitWidthProperty().bind(rightPane.widthProperty().multiply(0.95))
        pdfPreviewImageView.fitHeightProperty().bind(rightPane.heightProperty().multiply(0.95))
        pdfPreviewImageView.x = 5.0
        pdfPreviewImageView.y = 5.0

        pdfPreviewImageView.isPreserveRatio = true
    }

    @FXML
    fun selectWatermark() {
        println("select watermark")
        val file: File? = fileChooser.showOpenDialog(currentWindow())
        if (file != null) {
            watermarkFile.text = file.absolutePath
            watermarkPdf = file
            watermarkFileSelect.selectionModel.clearSelection()
            watermarkFileSelect.disableProperty().set(true)
        }
    }

    @FXML
    fun selectPdfDirs() {
        println("select selectPdfDirs")
        val file: File? = directoryChooser.showDialog(currentWindow())
        if (file != null) {
            watermarkFile.text = file.absolutePath
            pdfDirs = file
            updatePdfList(file)

        } else {
            clearPdfList()
        }
    }

    fun clearPdfList() {
        pdfFiles.clear()
        observablePdfList.clear()
    }

    fun updatePdfList(pdfDir: File) {
        clearPdfList()
        pdfFiles.addAll(pdfDirConverter.listPdfInDir(pdfDir).map { RelativeFileName(it, pdfDir) })
        observablePdfList.addAll(pdfFiles)
        pdfListView.items = observablePdfList

    }

    private fun renderPdfFile(selectedItem: File) {
        val watermarkPdfComputedFile = watermarkFile() ?: return
        val watermarkPdfComputed = WatermarkCreator().convertWatermarkIfNeeded(watermarkPdfComputedFile)
        val waterMarker = WaterMarker(selectedItem)

        val waterMarkePdf = waterMarker.waterMarkePdf(watermarkPdfComputed)
        val pdfToImageRenderer = PdfToImageRenderer(waterMarkePdf)
        val render: BufferedImage? = pdfToImageRenderer.render(0)
        waterMarker.close()
        val convertToFxImage = SwingFXUtils.toFXImage(render, null)
        if (convertToFxImage != null) {
            pdfPreviewImageView.image = convertToFxImage

        }
    }


    @FXML
    fun watermarkPdfs() {

        println("select watermarkPdfs")
        println("gui thread"+Thread.currentThread().name)




        val task: Task<Void> = object : Task<Void>() {
            @Throws(Exception::class)
            override fun call(): Void? {
                if (pdfDirs != null && watermarkFile() != null) {
                    pdfDirConverter.watermark(pdfDirs!!, watermarkFile()!!)

                }
                return null
            }
        }
        task.stateProperty().addListener(object : ChangeListener<Worker.State> {
            override fun changed(
                observable: ObservableValue<out Worker.State>,
                oldValue: Worker.State, newState: Worker.State
            ) {
                if (newState === Worker.State.SUCCEEDED) {
                    println("DONE")
                }
            }
        })
        val callable = Executors.callable(task)
        newSingleThreadExecutor.invokeAll(listOf(callable))
    }

    fun currentWindow(): Window = watermarkFile.scene.window


    fun watermarkFile(): File? {
        val watermarkFile = listOf(watermarkPdf, watermarkFileSelect.value).firstNotNullOf { it }
        return watermarkFile
    }
}
