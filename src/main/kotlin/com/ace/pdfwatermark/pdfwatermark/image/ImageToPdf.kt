package com.ace.pdfwatermark.pdfwatermark.image

import org.apache.batik.transcoder.TranscoderInput
import org.apache.batik.transcoder.TranscoderOutput
import org.apache.fop.svg.PDFTranscoder
import org.apache.pdfbox.io.IOUtils
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject
import java.io.File
import java.io.IOException

class ImageToPdf {

    fun createPdfFromSvg(svgFile: File, pdfFile: File) {
        val transcoderInput = TranscoderInput(svgFile.inputStream())
        val outputStream = pdfFile.outputStream()
        val transcoderOutput = TranscoderOutput(outputStream)
        val transcoder = PDFTranscoder()

        transcoder.transcode(transcoderInput, transcoderOutput)
        IOUtils.closeQuietly(outputStream)
    }

    fun createPdfFromImages(targetFile: File, images: List<File>) {
        val pdDocument = PDDocument()
        images.forEach {
            addImageAsPage(it.absolutePath, pdDocument)
        }
        pdDocument.save(targetFile)
    }

    @Throws(IOException::class)
    fun addImageAsPage(imagePath: String, doc: PDDocument) {

        val pageRectangle = PDRectangle.A4
        val page = PDPage(pageRectangle)
        doc.addPage(page)

        val pdImage = PDImageXObject.createFromFile(imagePath, doc)
        PDPageContentStream(doc, page, PDPageContentStream.AppendMode.APPEND, true, true).use { contentStream ->
            val scaleW = (pageRectangle.width / pdImage.width)
            val scaleH = (pageRectangle.height / pdImage.height)
            contentStream.drawImage(pdImage, 0f, 0f, pdImage.width * scaleW, pdImage.height * scaleH)
        }
    }
}
