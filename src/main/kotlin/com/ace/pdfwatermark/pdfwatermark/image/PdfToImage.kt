package com.ace.pdfwatermark.pdfwatermark.image

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.tools.imageio.ImageIOUtil
import java.io.File
import java.io.IOException

class PdfToImage {
    @Throws(IOException::class)
    fun convert(pdf: File, relativeFilename: String, output: File):List<File> {
        val imageFiles= mutableListOf<File>()
        val imageFormat = "png"
        val outputPrefix = relativeFilename
        var document: PDDocument? = null
        try {
            document = PDDocument.load(pdf)
            val pdfToImageRenderer = PdfToImageRenderer(document)
            val endPage = document.numberOfPages
            var success = true
            for (i in 0 until endPage) {
                val image = pdfToImageRenderer.render(i)

                val fileName = outputPrefix +"_" +(i + 1) + "." + imageFormat
                val imageFile = File(output, fileName)
                imageFile.parentFile.mkdirs()
                success =
                    success and ImageIOUtil.writeImage(image, imageFile.absolutePath, pdfToImageRenderer.dpi, 0.9f)

                imageFiles.add(imageFile)
            }
            if (!success) {
                System.err.println(
                    "Error: no writer found for image format '"
                            + imageFormat + "'"
                )
            }
        } finally {
            document?.close()
        }
        return imageFiles
    }
}
