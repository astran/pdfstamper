package com.ace.pdfwatermark.pdfwatermark.image

import com.ace.pdfwatermark.pdfwatermark.logTime
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.rendering.ImageType
import org.apache.pdfbox.rendering.PDFRenderer
import java.awt.HeadlessException
import java.awt.Toolkit
import java.awt.image.BufferedImage
import java.io.IOException

class PdfToImageRenderer(val document: PDDocument) {
    val dpi: Int
    val renderer: PDFRenderer

    val imageType = ImageType.RGB
    init {
        dpi = try {
            Toolkit.getDefaultToolkit().screenResolution
        } catch (e: HeadlessException) {
            96
        }
        val acroForm = document.documentCatalog.acroForm
        if (acroForm != null && acroForm.needAppearances) {
            acroForm.refreshAppearances()
        }
        renderer = PDFRenderer(document)
        renderer.isSubsamplingAllowed = false
    }

    fun render(page: Int): BufferedImage? {
        val endPage = document.numberOfPages
        if (page > endPage) {
            return null
        }

        val image = logTime(" render page $page "){renderer.renderImageWithDPI(page, dpi.toFloat(), imageType)}
        return image

    }


}
