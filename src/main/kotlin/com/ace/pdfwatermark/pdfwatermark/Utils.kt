package com.ace.pdfwatermark.pdfwatermark

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.security.DigestInputStream
import java.security.MessageDigest
import java.util.concurrent.Callable
import kotlin.system.measureTimeMillis

fun File.sha1():String{
    return checksum(this.absolutePath,md)
}
fun File.shortSha1():String{
    return sha1().substring(0,10)
}
var md = MessageDigest.getInstance("SHA-1")
@Throws(IOException::class)
private fun checksum(filepath: String, md: MessageDigest): String {

    // file hashing with DigestInputStream
    var md = md
    DigestInputStream(FileInputStream(filepath), md).use { dis ->
        while (dis.read() != -1);
        md = dis.messageDigest
    }

    // bytes to hex
    val result = StringBuilder()
    for (b in md.digest()) {
        result.append(String.format("%02x", b))
    }
    return result.toString()
}

fun<V> logTime(message:String="exec time: ",block: ()->V):V{
    var v:V
    val time = measureTimeMillis {
        v = block()
    }
    println("$message $time ms")

    return v
}
