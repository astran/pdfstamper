package com.ace.pdfwatermark.pdfwatermark

import org.apache.pdfbox.multipdf.Overlay
import org.apache.pdfbox.pdmodel.PDDocument
import java.io.File
import java.io.IOException

class WaterMarker(val pdfFile: File) {
    var realDoc: PDDocument? = null
    var overlayedPdf: PDDocument? = null
    var overlay: Overlay? = null

    fun close() {
        realDoc?.close()
        overlayedPdf?.close()
        overlay?.close()
    }

    @Throws(IOException::class)
    fun waterMarkePdf(watermarkFile: File): PDDocument {

        realDoc = PDDocument.load(pdfFile)

        overlay = Overlay()
        overlay!!.setOverlayPosition(Overlay.Position.FOREGROUND)

        overlay!!.setInputPDF(realDoc)
        overlay!!.setAllPagesOverlayFile(watermarkFile.absolutePath)

        overlayedPdf = overlay!!.overlayDocuments(mapOf())

        return overlayedPdf!!


    }

}
