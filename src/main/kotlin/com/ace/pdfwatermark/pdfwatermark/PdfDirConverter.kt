package com.ace.pdfwatermark.pdfwatermark

import com.ace.pdfwatermark.pdfwatermark.image.ImageToPdf
import com.ace.pdfwatermark.pdfwatermark.image.PdfToImage
import com.ace.pdfwatermark.pdfwatermark.watermark.WatermarkCreator
import com.ace.pdfwatermark.pdfwatermark.watermark.WatermarkedPdf
import org.apache.pdfbox.io.MemoryUsageSetting
import org.apache.pdfbox.multipdf.PDFMergerUtility
import java.io.File
import java.io.IOException
import java.util.*
import java.util.stream.Collectors
import java.util.stream.Stream
import kotlin.streams.toList
import kotlin.system.measureTimeMillis


class PdfDirConverter {
    fun listPdfInDir(pdfDir: File): List<File> =
        Arrays.stream(pdfDir.listFiles()!!.sortedArrayWith(compareBy({ it.isDirectory }, { it.name })))
            .flatMap { if (it.isFile) Stream.of(it) else listPdfInDir(it).stream() }
            .filter { f: File -> f.name.endsWith(".pdf") }.collect(Collectors.toList())


    fun watermark(pdfDir: File, watermarkFileImage: File) {
        val saveDir = File(pdfDir.parentFile, pdfDir.name + "_export")
        val pdfFiles = listPdfInDir(pdfDir)
        val watermarkedPdfs = pdfFiles.map { pdf: File -> WatermarkedPdf(pdf, pdfDir) }
        watermark(watermarkedPdfs, saveDir, watermarkFileImage,true)


    }

    @Throws(Exception::class)
    fun watermark(pdfs: List<WatermarkedPdf>, exportDir: File, watermarkFileImage: File,generateImages:Boolean) {
        println("Main thread "+Thread.currentThread().name)
        val watermarkFile: File = WatermarkCreator().convertWatermarkIfNeeded(watermarkFileImage)
        val watermarkedPdfsDirectory = File(exportDir, "pdf")
        watermarkedPdfsDirectory.mkdirs()
        val saveImages = File(exportDir, "img")
        saveImages.mkdirs()

        val measureTimeMillis = measureTimeMillis {
            val wmPdfs=generatePdfs(pdfs, watermarkFile, watermarkedPdfsDirectory, exportDir)
            if(generateImages){
                generateImages(wmPdfs, saveImages, exportDir)
            }

        }
        println("global exec time : $measureTimeMillis")

    }

    private fun generatePdfs(
        pdfs: List<WatermarkedPdf>,
        watermarkFile: File,
        watermarkedPdfsDirectory: File,
        exportDir: File
    ):List<WatermarkedPdf> {
        val watermarkedPdfs = pdfs.parallelStream().map { pdf: WatermarkedPdf ->
            println("in parallel " + Thread.currentThread().name)

            logTime("pdf gen exec time :") { watermarkFile(pdf, watermarkFile, watermarkedPdfsDirectory) }
        }.toList()
        mergeAllPdfs(exportDir, watermarkedPdfs)
        return watermarkedPdfs
    }

    private fun generateImages(
        pdfs: List<WatermarkedPdf>,
        saveImages: File,
        exportDir: File
    ):List<WatermarkedPdf> {
        val pdfToImage = PdfToImage()
        val watermarkedPdfs = pdfs.parallelStream().map { pdf: WatermarkedPdf ->

            logTime("pdf to image exec time :") { convertPdfToImages(pdf, pdfToImage, saveImages) }
        }.toList()
        createPdfFromImages(exportDir, watermarkedPdfs)
        return watermarkedPdfs
    }

    private fun convertPdfToImages(
        pdf: WatermarkedPdf,
        pdfToImage: PdfToImage,
        saveImages: File
    ): WatermarkedPdf {
        val relativeFilename = pdf.relativeFileName()
        val filesToImage = pdfToImage.convert(pdf.pdfWithWatermark!!, relativeFilename, saveImages)
        return pdf.copy(watermarkedImages = filesToImage)
    }

    @Throws(IOException::class)
    private fun watermarkFile(
        pdf: WatermarkedPdf,
        watermarkFile: File,
        watermarkedPdfsDirectory: File
    ): WatermarkedPdf {

        val relativeFilename = pdf.relativeFileName()
        val waterMarker = WaterMarker(pdf.source)
        val watermarkedPdf = waterMarker.waterMarkePdf(watermarkFile)
        val wmPdf = File(watermarkedPdfsDirectory, relativeFilename + "_wm.pdf")
        wmPdf.parentFile.mkdirs()
        watermarkedPdf.save(wmPdf)
        waterMarker.close()
        return pdf.copy(pdfWithWatermark = wmPdf)

    }

    private fun createPdfFromImages(saveDir: File, watermarkedPdfs: List<WatermarkedPdf>) {
        ImageToPdf().createPdfFromImages(
            File(saveDir, "ALL_DOCS_MERGED_IMG.pdf"),
            watermarkedPdfs.flatMap { pdf -> pdf.watermarkedImages!! })
    }

    private fun mergeAllPdfs(
        saveDir: File,
        watermarkedPdfs: List<WatermarkedPdf>
    ) {
        val pdfMerger = PDFMergerUtility()
        pdfMerger.destinationFileName = File(saveDir, "ALL_DOCS_MERGED.pdf").absolutePath
        watermarkedPdfs.flatMap { pdf -> listOf(pdf.pdfWithWatermark!!) }.forEach(pdfMerger::addSource)
        pdfMerger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly())
    }


}
